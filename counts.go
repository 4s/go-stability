package stability

// Counts holds the numbers of requests and their successes/failures.
// CircuitBreaker clears the internal Counts either
// on the change of the state or at the closed-state intervals.
// Counts ignores the results of the requests sent before clearing.
type Counts struct {
	Requests             uint32
	TotalSuccesses       uint32
	TotalFailures        uint32
	ConsecutiveSuccesses uint32
	ConsecutiveFailures  uint32
}

// onRequest executes the actions that should be executed on requests. It increments the Requests
func (c *Counts) onRequest() {
	c.Requests++
}

// onSuccess executes the actions that should be executed on success. It increments the
// TotalSuccesses and ConsecutiveSuccesses and resets ConsecutiveFailures
func (c *Counts) onSuccess() {
	c.TotalSuccesses++
	c.ConsecutiveSuccesses++
	c.ConsecutiveFailures = 0
}

// onFailure executes the actions that should be executed on failure. It increments the
// TotalFailures and ConsecutiveFailures and resets ConsecutiveSuccesses
func (c *Counts) onFailure() {
	c.TotalFailures++
	c.ConsecutiveFailures++
	c.ConsecutiveSuccesses = 0
}

// clear clears all counts for the Counts
func (c *Counts) clear() {
	c.Requests = 0
	c.TotalSuccesses = 0
	c.TotalFailures = 0
	c.ConsecutiveSuccesses = 0
	c.ConsecutiveFailures = 0
}
