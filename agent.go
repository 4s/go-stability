package stability

// Agent is a struct used to make more resilient requests. It has the possibility to implement
// both the circuitbreaker and the retry stability pattern
type Agent struct {
	CircuitBreaker *CircuitBreaker
	Retry          *Retry
}

// NewAgent functions as a constructor for Agent that returns a new instance of Agent
func NewAgent(name string) Agent {
	circuitBreaker := NewCircuitBreaker(name + " (CB)")
	retry := NewRetry(name + " (Retry)")

	agent := Agent{
		circuitBreaker,
		retry,
	}

	return agent
}

// SetCircuitBreaker sets the CircuitBreaker of the Agent
func (agent *Agent) SetCircuitBreaker(circuitBreaker *CircuitBreaker) {
	agent.CircuitBreaker = circuitBreaker
}

// SetRetry sets the Retry of the Agent
func (agent *Agent) SetRetry(retry *Retry) {
	agent.Retry = retry
}

// Do is the main function of Agent and executes the request using the circuit breaker and/or
// the retry if they exist.
func (agent Agent) Do(req func() error) error {

	if agent.CircuitBreaker != nil {
		req = agent.CircuitBreaker.Do(req)
	}
	if agent.Retry != nil {
		req = agent.Retry.Do(req)
	}

	return req()
}
