package stability

import (
	"errors"
	"time"
)

// RetryableFunc is the function signature of a function that is retryable by this library
type RetryableFunc func() error

// OnRetryFunc is the function signature of the type of function that can be called on retries.
type OnRetryFunc func(n uint, err error)

// RetryIfFunc is the function signature of the type of function that can be used to determine
// whether or not the retryable function should be retried upon failure.
type RetryIfFunc func(error) bool

// Retry is a struct representing the retry functionality.
type Retry struct {
	// The name of the Retry
	name string
	// The amount of attempts the Retry struct should make, before returning an error.
	attempts uint
	// The initial delay in ms before attempting another retry. Delay is doubled for each consecutive retry.
	delay time.Duration
	// A function that is called each time a retry occurs.
	onRetry OnRetryFunc
	// retryIf is a function that is used to determine whether the retryable function should be retried.
	retryIf RetryIfFunc
}

var (
	// ErrRequestFailed is returned when a request has reached the maximum number of retries, and still fails.
	ErrRequestFailed = errors.New("RequestFailed")
)

// NewRetry functions as a constructor for Retry that returns a new instance of Retry
func NewRetry(name string) *Retry {
	retry := new(Retry)

	retry.name = name
	// attempts is set to 10 by default
	retry.attempts = 10
	// delay is set to 200 by default.
	retry.delay = 200
	// retryIf is set to a function that always returns true by default.
	retry.retryIf = func(error) bool { return true }

	return retry
}

// SetAttempts is a method for setting the amount of attempts for Retry.
// If set to 0, there will be no limit to the amount of retries.
func (retry *Retry) SetAttempts(attempts uint) {
	retry.attempts = attempts
}

// SetDelay is a method for setting the initial delay in ms of Retry
func (retry *Retry) SetDelay(delay time.Duration) {
	retry.delay = delay
}

// SetOnRetry is a method for setting the onRetry function of Retry.
func (retry *Retry) SetOnRetry(onRetry func(n uint, err error)) {
	retry.onRetry = onRetry
}

// SetRetryIf is a method for setting the retryIf function of Retry.
func (retry *Retry) SetRetryIf(retryIf func(error) bool) {
	retry.retryIf = retryIf
}

// Do is the main method of Retry and executes and retries the retryable function
func (retry *Retry) Do(retryableFunc RetryableFunc) func() error {
	return func() error {
		var try uint

		maxTries := retry.attempts
		if maxTries == 0 {
			maxTries = 1
		}

		for try < maxTries {
			err := retryableFunc()
			if err != nil {
				if retry.onRetry != nil {
					retry.onRetry(try, err)
				}
				// TODO: do something with error

				if !retry.retryIf(err) {
					break
				}

				// if this is last attempt - don't wait
				if try == retry.attempts-1 {
					break
				}

				// set new delay
				delayTime := retry.delay * (1 << (try - 1))
				if delayTime > 10*time.Second {
					delayTime = 10 * time.Second
				}
				// sleep until next retry
				time.Sleep((time.Duration)(delayTime) * time.Millisecond)
			} else {
				return nil
			}
			if retry.attempts != 0 {
				try++
			}
		}
		return ErrRequestFailed
	}
}
