package stability

import (
	"errors"
)

// State is a type that represents a state of CircuitBreaker.
type State int

// These constants are states of CircuitBreaker.
const (
	StateClosed State = iota
	StateHalfOpen
	StateOpen
)

var (
	// ErrTooManyRequests is returned when the CB state is half open and the requests count is over
	// the cb maxRequests
	ErrTooManyRequests = errors.New("too many requests")
	// ErrOpenState is returned when the CB state is open
	ErrOpenState = errors.New("circuit breaker is open")
)

// String returns a stringified version of the state
func (s State) String() (string, error) {
	switch s {
	case StateClosed:
		return "closed", nil
	case StateHalfOpen:
		return "half-open", nil
	case StateOpen:
		return "open", nil
	default:
		return "", errors.New("unknown state: " + string(s))
	}
}
