package mocks

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

// MockClient is a mock of a HttpClient, used to return a predetermined response for testing.
// DoFunc is the function that is executed when the Do-method is called on the  MockClient.
// If no DoFunc is set for the MockClient, the MockClient returns a standard 200-response
type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

// Do "executes" the request passed to it. If the DoFunc is set, it executes the DoFunc with the
// request as a argument. Otherwise it returns a 200-response.
func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	if m.DoFunc != nil {
		return m.DoFunc(req)
	}

	body := "Hello world"
	response := http.Response{
		Status:        "200 OK",
		StatusCode:    200,
		Proto:         "HTTP/1.1",
		ProtoMajor:    1,
		ProtoMinor:    1,
		Body:          ioutil.NopCloser(bytes.NewBufferString(body)),
		ContentLength: int64(len(body)),
		Request:       req,
		Header:        make(http.Header, 0),
	}
	return &response, nil
}
