package stability

import (
	"errors"
	"net/http"
	"strconv"
	"testing"

	"bitbucket.org/4s/go-stability/mocks"
)

func TestRetryDo(t *testing.T) {

	tests := map[string]struct {
		attempts       uint
		failures       uint
		expectedTries  uint
		expectedStatus int
		expectedError  error
	}{
		"\"Retry should not retry request, if request succeeds \"": {
			attempts:       5,
			failures:       0,
			expectedTries:  1,
			expectedStatus: 200,
			expectedError:  nil,
		},
		"\"Retry should retry request, if request fails and attempts is not exceeded \"": {
			attempts:       5,
			failures:       3,
			expectedTries:  4,
			expectedStatus: 200,
			expectedError:  nil,
		},
		"\"An error should be thrown when consecutive failures exceeds attempts\"": {
			attempts:       5,
			failures:       6,
			expectedTries:  5,
			expectedStatus: 500,
			expectedError:  ErrRequestFailed,
		},
		"\"Retry should continue retrying until the request is successful, if attempts is set to 0\"": {
			attempts:       0,
			failures:       20,
			expectedTries:  21,
			expectedStatus: 200,
			expectedError:  nil,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)

		retry := NewRetry("Retry")
		retry.attempts = test.attempts

		var try uint
		client := &mocks.MockClient{
			DoFunc: func(req *http.Request) (*http.Response, error) {
				try++
				var response http.Response
				if try <= test.failures {
					response = http.Response{
						Status:     strconv.Itoa(500) + " Bad Request",
						StatusCode: 500,
						Request:    req,
					}
				} else {
					response = http.Response{
						Status:     strconv.Itoa(200) + " Success",
						StatusCode: 200,
						Request:    req,
					}
				}
				return &response, nil
			},
		}
		var resp *http.Response
		outcomeError := retry.Do(
			func() error {
				req, err := http.NewRequest(
					"GET",
					"https://httpstat.us/500",
					nil,
				)
				if err != nil {
					return err
				}

				resp, err = client.Do(req)
				if err != nil {
					return err
				}
				if resp.StatusCode < 200 || resp.StatusCode > 299 {
					return errors.New(resp.Status)
				}
				return nil
			},
		)()
		if try != test.expectedTries {
			t.Errorf("amount of tries should be \"%v\", but is \"%v\"", test.expectedTries, try)
		}
		if outcomeError != nil && outcomeError != test.expectedError {
			t.Errorf("error should be \"%v\", but is \"%v\"", test.expectedError, outcomeError)
		}
		if resp.StatusCode != test.expectedStatus {
			t.Errorf("statusCode should be \"%v\", but is \"%v\"", test.expectedStatus, resp.StatusCode)
		}
	}
}
