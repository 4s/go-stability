# Go Stability library

A library that allows developers to use the Circuitbreaker- and Retry-pattern on requests.

## Project wiki
See: [Main.md](docs//Main.md)

## Project status
4S maturity level: prototyping.

## Prerequisites
You need to have the following software installed

Go (golang) (https://golang.org/)
librdkafka (https://github.com/confluentinc/confluent-kafka-go#installing-librdkafka)

## Building:

This library does not contain a main package, and therefore cannot be built on its own.

## Usage

To use this library, it must be included in a project containing a main-package.

To include this library run the following command in your terminal/shell:

```bash
go get "bitbucket.org/4s/go-stability"
```

After retrieving the library, the library can be included by importing it, in the files in which it is required:

```golang
package ...
import (
  ...
  stability "bitbucket.org/4s/go-stability"
)
...
agent := stability.NewAgent("Name")
client := http.Client{}

err := agent.Do(
    func() error {
        // create request
        req, err := http.NewRequest(
            "GET",
            "http://example.org",
            nil,
        )
        if err != nil {
            // handle error
        }
        // execute request and get response
        resp, err := client.Do(req)
        if err != nil {
            // handle error
        }
        defer resp.Body.Close()

        // handle response
        ...
        return nil
    },
)
...
```

## Environment variables
This library uses no environment variables

## Test
__To run unit tests:__
```
go test
```