package stability

import (
	"sync"
	"time"
)

// CircuitBreaker is a state machine to prevent sending requests that are likely to fail.
type CircuitBreaker struct {
	// The name of the circuitbreaker
	name string
	// The maximum amount of requests allowed while the circuitbreaker is in half-open state
	maxRequests uint32
	// The cyclic period of the closed state for the CircuitBreaker to clear the internal Counts.
	// If set to zero the internal counts will not be cleared.
	interval time.Duration
	// The period of the open state, after which the state of the CircuitBreaker becomes half-open
	timeout time.Duration
	// The function for determining whether the circuitbreaker is ready to transition into open state.
	readyToTrip func(counts Counts) bool
	// The function called when the CircuitBreaker changes state
	onStateChange func(from State, to State)
	// Used to avoid concurrency issues
	mutex sync.Mutex
	// The current state of the circuitbreaker
	state State
	// An integer that increments on each state change (or count reset)
	generation uint64
	// An object that keeps track of the different counts, to allow for the circuit to break and reopen
	counts Counts
	// The time in which a generation expires - only relevant if interval is set
	expiry time.Time
}

// NewCircuitBreaker functions as a constructor for CircuitBreaker that returns a new instance of
// CircuitBreaker
func NewCircuitBreaker(name string) *CircuitBreaker {
	cb := new(CircuitBreaker)

	cb.name = name
	// maxRequests is set to 1 by default
	cb.maxRequests = 1
	// interval is set to 0 by default
	cb.interval = 0
	// timeout is set to 60 seconds by default
	cb.timeout = time.Duration(60) * time.Second
	// readyToTrip is set to the function "DefaultReadyToTrip" by default
	cb.readyToTrip = DefaultReadyToTrip

	return cb
}

// SetMaxRequests the maximum amount of requests allowed while the circuitbreaker is in half-open state
func (cb *CircuitBreaker) SetMaxRequests(maxRequests uint32) {
	cb.maxRequests = maxRequests
}

// SetInterval sets the cyclic period of the closed state for the CircuitBreaker to clear the internal Counts.
// If set to zero the internal counts will not be cleared.
func (cb *CircuitBreaker) SetInterval(interval time.Duration) {
	cb.interval = interval
}

// SetTimeout sets the period of the open state, after which the state of the CircuitBreaker becomes half-open
func (cb *CircuitBreaker) SetTimeout(timeout time.Duration) {
	cb.timeout = timeout
}

// SetReadyToTrip sets the function for determining whether the circuitbreaker is ready to transition into open state.
func (cb *CircuitBreaker) SetReadyToTrip(readyToTrip func(counts Counts) bool) {
	cb.readyToTrip = readyToTrip
}

// SetOnStateChange sets the function called when the CircuitBreaker changes state
func (cb *CircuitBreaker) SetOnStateChange(onStateChange func(from State, to State)) {
	cb.onStateChange = onStateChange
}

// Do executes the given request if the CircuitBreaker accepts it.
// Do returns an error instantly if the CircuitBreaker rejects the request.
// Otherwise, Do returns the result of the request.
// If a panic occurs in the request, the CircuitBreaker handles it as a failed request
// and causes the same panic again.
func (cb *CircuitBreaker) Do(req func() error) func() error {
	return func() error {
		// check if request should be executed
		err := cb.beforeRequest()
		if err != nil {
			return err
		}
		before := cb.generation
		// handle potential panic
		defer func() {
			e := recover()
			if e != nil {
				cb.afterRequest(before, false)
				panic(e)
			}
		}()
		// perform request
		err = req()
		// update state after request
		cb.afterRequest(before, err == nil)
		if err != nil {
			return err
		}
		return nil
	}
}

// beforeRequest tests whether the circuit should return immediately without executing the request.
// If the state of the CircuitBreaker is open, it will return a ErrOpenState error
// If the state is half-open and the amount of requests have exceeded the maxRequests variable,
// it will return a ErrTooManyRequests error
func (cb *CircuitBreaker) beforeRequest() error {
	cb.mutex.Lock()
	defer cb.mutex.Unlock()

	now := time.Now()
	cb.verifyState(now)

	if cb.state == StateOpen {
		return ErrOpenState
	} else if cb.state == StateHalfOpen && cb.counts.Requests >= cb.maxRequests {
		return ErrTooManyRequests
	}

	return nil
}

// afterRequest checks whether the current state has expired, and updates the counts if it has not.
// If the current state has not expired, afterRequest calls either onSuccess or onFailure, depending
// on the result of the request.
// If the current state has expired the method returns, as the counts have been reset and the
// success or failure of the request should not impact the new counts.
func (cb *CircuitBreaker) afterRequest(before uint64, success bool) {
	cb.mutex.Lock()
	defer cb.mutex.Unlock()

	now := time.Now()
	cb.verifyState(now)
	if cb.generation != before {
		return
	}

	if success {
		cb.onSuccess(now)
	} else {
		cb.onFailure(now)
	}
}

// onSuccess executes all actions that should be executed upon a successfull request.
// onSuccess should be called each time a request is successfull
func (cb *CircuitBreaker) onSuccess(now time.Time) {
	switch cb.state {
	case StateClosed:
		cb.counts.onSuccess()
	case StateHalfOpen:
		cb.counts.onSuccess()
		if cb.counts.ConsecutiveSuccesses >= cb.maxRequests {
			cb.setState(StateClosed, now)
		}
	}
}

// onFailure executes all actions that should be executed upon a failed request.
func (cb *CircuitBreaker) onFailure(now time.Time) {
	switch cb.state {
	case StateClosed:
		cb.counts.onFailure()
		if cb.readyToTrip(cb.counts) {
			cb.setState(StateOpen, now)
		}
	case StateHalfOpen:
		cb.setState(StateOpen, now)
	}
}

// verifyState returns (and possibly sets) the current state of the CircuitBreaker, based on the
// previous state as well as the current time.
func (cb *CircuitBreaker) verifyState(now time.Time) {
	switch cb.state {
	case StateClosed:
		if !cb.expiry.IsZero() && cb.expiry.Before(now) {
			cb.toNewGeneration(now)
		}
	case StateOpen:
		if cb.expiry.Before(now) {
			cb.setState(StateHalfOpen, now)
		}
	}
}

// setState sets the state of the circuitbreaker and calls the onStateChange function.
// on every state change, the toNewGeneration is called, which resets the counts and expiry for the
// state
func (cb *CircuitBreaker) setState(state State, now time.Time) {
	if cb.state == state {
		return
	}
	_, err := state.String()
	if err != nil {
		return
	}

	prev := cb.state
	cb.state = state

	cb.toNewGeneration(now)

	if cb.onStateChange != nil {
		cb.onStateChange(prev, state)
	}
}

// toNewGeneration resets counts and expiry for all states
// If the current state is closed and no interval has been set, the expiry is set to 0.
// If the current state is closed and interval has been set, expiry is set to now + interval.
// If current state is open, expiry is set to now + timeout
// If current state is half-open expiry is set to 0
func (cb *CircuitBreaker) toNewGeneration(now time.Time) {
	cb.generation++
	cb.counts.clear()

	var zero time.Time
	switch cb.state {
	case StateClosed:
		if cb.interval == 0 {
			cb.expiry = zero
		} else {
			cb.expiry = now.Add(cb.interval)
		}
	case StateOpen:
		cb.expiry = now.Add(cb.timeout)
	default: // StateHalfOpen
		cb.expiry = zero
	}
}

// DefaultReadyToTrip is the default readyToTrip function, which returns true if the
// ConsecutiveFailures count is 5 or more, and false otherwise
func DefaultReadyToTrip(counts Counts) bool {
	if counts.ConsecutiveFailures < 5 {
		return false
	}
	return true
}
