package stability

import (
	"bytes"
	"errors"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/4s/go-stability/mocks"
)

func TestDefaultReadyToTrip(t *testing.T) {
	tests := map[string]struct {
		consecutiveFailures uint32
		expected            bool
	}{
		"\"consecutiveFailures: 1\"": {
			consecutiveFailures: 1,
			expected:            false,
		},
		"\"consecutiveFailures: 5\"": {
			consecutiveFailures: 5,
			expected:            true,
		},
		"\"consecutiveFailures: 10\"": {
			consecutiveFailures: 10,
			expected:            true,
		},
		"\"consecutiveFailures: MaxInt32\"": {
			consecutiveFailures: math.MaxInt32,
			expected:            true,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		counts := Counts{}
		counts.ConsecutiveFailures = test.consecutiveFailures

		output := DefaultReadyToTrip(counts)
		if output != test.expected {
			t.Errorf("DeafultReadyToTrip returned: %v, output should be: %v", output, test.expected)
		}
	}
}

func TestToNewGeneration(t *testing.T) {
	tests := map[string]struct {
		generation        uint64
		interval          time.Duration
		timeout           time.Duration
		state             State
		outputGeneration  uint64
		outputExpiryAdded time.Duration
	}{
		"\"Interval: zero, state: closed\"": {
			generation:        0,
			interval:          0,
			timeout:           time.Duration(60) * time.Second,
			state:             StateClosed,
			outputGeneration:  1,
			outputExpiryAdded: 0,
		},
		"\"Interval: 5 seconds, state: closed\"": {
			generation:        0,
			interval:          time.Duration(5) * time.Second,
			timeout:           time.Duration(60) * time.Second,
			state:             StateClosed,
			outputGeneration:  1,
			outputExpiryAdded: time.Duration(5) * time.Second,
		},
		"\"Interval: zero, state: open\"": {
			generation:        0,
			interval:          0,
			timeout:           time.Duration(60) * time.Second,
			state:             StateOpen,
			outputGeneration:  1,
			outputExpiryAdded: time.Duration(60) * time.Second,
		},
		"\"Interval: 5 seconds, state: open\"": {
			generation:        0,
			interval:          time.Duration(5) * time.Second,
			timeout:           time.Duration(60) * time.Second,
			state:             StateOpen,
			outputGeneration:  1,
			outputExpiryAdded: time.Duration(60) * time.Second,
		},
		"\"Interval: zero, state: half-open\"": {
			generation:        0,
			interval:          0,
			timeout:           time.Duration(60) * time.Second,
			state:             StateHalfOpen,
			outputGeneration:  1,
			outputExpiryAdded: 0,
		},
		"\"Interval: 5 seconds, state: half-open\"": {
			generation:        0,
			interval:          time.Duration(5) * time.Second,
			timeout:           time.Duration(60) * time.Second,
			state:             StateHalfOpen,
			outputGeneration:  1,
			outputExpiryAdded: 0,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.generation = test.generation
		cb.interval = test.interval
		cb.timeout = test.timeout
		cb.state = test.state

		counts := Counts{
			Requests:             10,
			TotalSuccesses:       10,
			TotalFailures:        10,
			ConsecutiveSuccesses: 10,
			ConsecutiveFailures:  0,
		}
		cb.counts = counts

		now := time.Now()
		var zero time.Time

		cb.toNewGeneration(now)

		if cb.generation != 1 {
			t.Error("generation was not incremented correctly")
		}
		if cb.counts.Requests != 0 {
			t.Errorf("counts.Requests was not 0. Was %v after toNewGeneration call", cb.counts.ConsecutiveFailures)
		}
		if cb.counts.TotalSuccesses != 0 {
			t.Errorf("counts.TotalSuccesses was not 0. Was %v after toNewGeneration call", cb.counts.ConsecutiveFailures)
		}
		if cb.counts.TotalFailures != 0 {
			t.Errorf("counts.TotalFailures was not 0. Was %v after toNewGeneration call", cb.counts.ConsecutiveFailures)
		}
		if cb.counts.ConsecutiveSuccesses != 0 {
			t.Errorf("counts.ConsecutiveSuccesses was not 0. Was %v after toNewGeneration call", cb.counts.ConsecutiveFailures)
		}
		if cb.counts.ConsecutiveFailures != 0 {
			t.Errorf("counts.ConsecutiveFailures was not 0. Was %v after toNewGeneration call", cb.counts.ConsecutiveFailures)
		}
		if test.outputExpiryAdded == 0 {
			if cb.expiry != zero {
				t.Errorf("cb.expiry should be %v, but is %v", zero, cb.expiry)
			}
		} else if cb.expiry != now.Add(test.outputExpiryAdded) {
			t.Errorf("cb.expiry should be %v, but is %v", now.Add(test.outputExpiryAdded), cb.expiry)
		}
	}
}

func TestSetState(t *testing.T) {
	tests := map[string]struct {
		from   State
		to     State
		output State
	}{
		"\"from: closed, to: open\"": {
			from:   StateClosed,
			to:     StateOpen,
			output: StateOpen,
		},
		"\"from: closed, to: half-open\"": {
			from:   StateClosed,
			to:     StateHalfOpen,
			output: StateHalfOpen,
		},
		"\"from: closed, to: closed\"": {
			from:   StateClosed,
			to:     StateClosed,
			output: StateClosed,
		},
		"\"from: closed, to: unknown\"": {
			from:   StateClosed,
			to:     State(5),
			output: StateClosed,
		},
		"\"from: open, to: closed\"": {
			from:   StateOpen,
			to:     StateClosed,
			output: StateClosed,
		},
		"\"from: open, to: half-open\"": {
			from:   StateOpen,
			to:     StateHalfOpen,
			output: StateHalfOpen,
		},
		"\"from: open, to: open\"": {
			from:   StateOpen,
			to:     StateOpen,
			output: StateOpen,
		},
		"\"from: open, to: unknown\"": {
			from:   StateOpen,
			to:     State(5),
			output: StateOpen,
		},
		"\"from: half-open, to: closed\"": {
			from:   StateHalfOpen,
			to:     StateClosed,
			output: StateClosed,
		},
		"\"from: half-open, to: open\"": {
			from:   StateHalfOpen,
			to:     StateOpen,
			output: StateOpen,
		},
		"\"from: half-open, to: half-open\"": {
			from:   StateHalfOpen,
			to:     StateHalfOpen,
			output: StateHalfOpen,
		},
		"\"from: half-open, to: unknown\"": {
			from:   StateHalfOpen,
			to:     State(89),
			output: StateHalfOpen,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.state = test.from
		cb.setState(test.to, time.Now())

		if cb.state != test.output {
			t.Errorf("cb.state should be %v, but is %v", test.output, cb.state)
		}
	}
}

func TestVerifyState(t *testing.T) {
	var zero time.Time
	tests := map[string]struct {
		state            State
		expiry           time.Time
		generation       uint64
		outputGeneration uint64
		outputState      State
	}{
		"\"state: closed, expiry: zero\"": {
			state:            StateClosed,
			expiry:           zero,
			generation:       0,
			outputGeneration: 0,
			outputState:      StateClosed,
		},
		"\"state: closed, expiry: after now\"": {
			state:            StateClosed,
			expiry:           time.Now().Add(time.Duration(1) * time.Hour),
			generation:       0,
			outputGeneration: 0,
			outputState:      StateClosed,
		},
		"\"state: closed, expiry: earlier than now\"": {
			state:            StateClosed,
			expiry:           time.Now().Add(time.Duration(-1) * time.Hour),
			generation:       0,
			outputGeneration: 1,
			outputState:      StateClosed,
		},
		"\"state: open, expiry: zero\"": {
			state:            StateOpen,
			expiry:           zero,
			generation:       0,
			outputGeneration: 1,
			outputState:      StateHalfOpen,
		},
		"\"state: open, expiry: after now\"": {
			state:            StateOpen,
			expiry:           time.Now().Add(time.Duration(1) * time.Hour),
			generation:       0,
			outputGeneration: 0,
			outputState:      StateOpen,
		},
		"\"state: open, expiry: earlier than now\"": {
			state:            StateOpen,
			expiry:           time.Now().Add(time.Duration(-1) * time.Hour),
			generation:       0,
			outputGeneration: 1,
			outputState:      StateHalfOpen,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.state = test.state
		cb.expiry = test.expiry
		cb.generation = test.generation

		cb.verifyState(time.Now())

		if cb.generation != test.outputGeneration {
			t.Errorf("cb.generation should be %v, but is %v", test.outputGeneration, cb.generation)
		}
		if cb.state != test.outputState {
			t.Errorf("cb.state should be %v, but is %v", test.outputState, cb.state)
		}
	}
}

func TestOnFailure(t *testing.T) {
	tests := map[string]struct {
		state               State
		consecutiveFailures uint32
		outputState         State
	}{
		"\"state: closed, consecutiveFailures: 1\"": {
			state:               StateClosed,
			consecutiveFailures: 1,
			outputState:         StateClosed,
		},
		"\"state: closed, consecutiveFailures: 4\"": {
			state:               StateClosed,
			consecutiveFailures: 4,
			outputState:         StateOpen,
		},
		"\"state: closed, consecutiveFailures: 10\"": {
			state:               StateClosed,
			consecutiveFailures: 10,
			outputState:         StateOpen,
		},
		"\"state: open, consecutiveFailures: 1\"": {
			state:               StateOpen,
			consecutiveFailures: 1,
			outputState:         StateOpen,
		},
		"\"state: open, consecutiveFailures: 4\"": {
			state:               StateOpen,
			consecutiveFailures: 4,
			outputState:         StateOpen,
		},
		"\"state: open, consecutiveFailures: 10\"": {
			state:               StateOpen,
			consecutiveFailures: 10,
			outputState:         StateOpen,
		},
		"\"state: half-open, consecutiveFailures: 1\"": {
			state:               StateHalfOpen,
			consecutiveFailures: 1,
			outputState:         StateOpen,
		},
		"\"state: half-open, consecutiveFailures: 4\"": {
			state:               StateHalfOpen,
			consecutiveFailures: 4,
			outputState:         StateOpen,
		},
		"\"state: half-open, consecutiveFailures: 10\"": {
			state:               StateHalfOpen,
			consecutiveFailures: 10,
			outputState:         StateOpen,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.state = test.state

		counts := Counts{
			Requests:             10,
			TotalSuccesses:       10,
			TotalFailures:        10,
			ConsecutiveSuccesses: 0,
			ConsecutiveFailures:  test.consecutiveFailures,
		}
		cb.counts = counts

		cb.onFailure(time.Now())

		if cb.state != test.outputState {
			t.Errorf("output state was: %v, expected: %v", cb.state, test.outputState)
		}
	}
}

func TestOnSuccess(t *testing.T) {
	tests := map[string]struct {
		state                State
		consecutiveSuccesses uint32
		outputState          State
	}{
		"\"state: closed, consecutiveSuccesses: 1\"": {
			state:                StateClosed,
			consecutiveSuccesses: 1,
			outputState:          StateClosed,
		},
		"\"state: closed, consecutiveSuccesses: 4\"": {
			state:                StateClosed,
			consecutiveSuccesses: 4,
			outputState:          StateClosed,
		},
		"\"state: closed, consecutiveSuccesses: 10\"": {
			state:                StateClosed,
			consecutiveSuccesses: 10,
			outputState:          StateClosed,
		},
		"\"state: open, consecutiveSuccesses: 1\"": {
			state:                StateOpen,
			consecutiveSuccesses: 1,
			outputState:          StateOpen,
		},
		"\"state: open, consecutiveSuccesses: 4\"": {
			state:                StateOpen,
			consecutiveSuccesses: 4,
			outputState:          StateOpen,
		},
		"\"state: open, consecutiveSuccesses: 10\"": {
			state:                StateOpen,
			consecutiveSuccesses: 10,
			outputState:          StateOpen,
		},
		"\"state: half-open, consecutiveSuccesses: 0\"": {
			state:                StateHalfOpen,
			consecutiveSuccesses: 0,
			outputState:          StateClosed,
		},
		"\"state: half-open, consecutiveSuccesses: 1\"": {
			state:                StateHalfOpen,
			consecutiveSuccesses: 1,
			outputState:          StateClosed,
		},
		"\"state: half-open, consecutiveSuccesses: 4\"": {
			state:                StateHalfOpen,
			consecutiveSuccesses: 4,
			outputState:          StateClosed,
		},
		"\"state: half-open, consecutiveSuccesses: 10\"": {
			state:                StateHalfOpen,
			consecutiveSuccesses: 10,
			outputState:          StateClosed,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.state = test.state
		cb.maxRequests = 1

		counts := Counts{
			Requests:             10,
			TotalSuccesses:       10,
			TotalFailures:        10,
			ConsecutiveSuccesses: 0,
			ConsecutiveFailures:  test.consecutiveSuccesses,
		}
		cb.counts = counts

		cb.onSuccess(time.Now())

		if cb.state != test.outputState {
			t.Errorf("output state was: %v, expected: %v", cb.state, test.outputState)
		}
	}
}

func TestBeforeRequest(t *testing.T) {
	tests := map[string]struct {
		state       State
		maxRequests uint32
		outputState State
		outputErr   error
	}{
		"\"Closed state as input should do nothing\"": {
			state:       StateClosed,
			maxRequests: 1,
			outputState: StateClosed,
			outputErr:   nil,
		},
		"\"Open state as input should result in ErrOpenState-error\"": {
			state:       StateOpen,
			maxRequests: 1,
			outputState: StateOpen,
			outputErr:   ErrOpenState,
		},
		"\"StateHalfOpen as input and maxRequests exceeded should result in ErrTooManyRequests-error\"": {
			state:       StateHalfOpen,
			maxRequests: 1,
			outputState: StateHalfOpen,
			outputErr:   ErrTooManyRequests,
		},
		"\"StateHalfOpen as input and maxRequests exceeded should do nothing\"": {
			state:       StateHalfOpen,
			maxRequests: 3,
			outputState: StateHalfOpen,
			outputErr:   nil,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)
		cb := NewCircuitBreaker("circuit breaker")
		cb.state = test.state
		cb.maxRequests = test.maxRequests
		cb.expiry = time.Now().Add(time.Duration(1) * time.Hour)
		cb.generation = 0
		cb.counts = Counts{
			Requests: 1,
		}

		err := cb.beforeRequest()
		if err != test.outputErr {
			t.Errorf("error should be \"%v\", but is \"%v\"", test.outputErr, err)
		}
		if cb.state != test.outputState {
			t.Errorf("cb.state should be %v, but is %v", test.outputState, cb.state)
		}
	}
}

func TestCircuitBreakerDo(t *testing.T) {

	tests := map[string]struct {
		state               State
		consecutiveFailures uint32
		expiry              time.Time
		requests            uint32
		status              int
		outputState         State
		outputErr           error
	}{
		"\"state: closed, consecutiveFailures: 0, expiry: after now, requests: 0, status: 200\"": {
			state:               StateClosed,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: closed, consecutiveFailures: 0, expiry: before now, requests: 0, status: 200\"": {
			state:               StateClosed,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(-1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: closed, consecutiveFailures: 5, expiry: before now, requests: 0, status: 200\"": {
			state:               StateClosed,
			consecutiveFailures: 5,
			expiry:              time.Now().Add(time.Duration(-1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: closed, consecutiveFailures: 10, expiry: before now, requests: 0, status: 200\"": {
			state:               StateClosed,
			consecutiveFailures: 10,
			expiry:              time.Now().Add(time.Duration(-1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: closed, consecutiveFailures: 10, expiry: before now, requests: 0, status: 500\"": {
			state:               StateClosed,
			consecutiveFailures: 10,
			expiry:              time.Now().Add(time.Duration(-1) * time.Hour),
			requests:            0,
			status:              500,
			outputState:         StateClosed,
			outputErr:           errors.New("500 Bad Request"),
		},
		"\"state: open, consecutiveFailures: 0, expiry: after now, requests: 0, status: 200\"": {
			state:               StateOpen,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateOpen,
			outputErr:           ErrOpenState,
		},
		"\"state: open, consecutiveFailures: 0, expiry: before now, requests: 0, status: 200\"": {
			state:               StateOpen,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(-1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: half-open, consecutiveFailures: 0, expiry: after now, requests: 0, status: 200\"": {
			state:               StateHalfOpen,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(1) * time.Hour),
			requests:            0,
			status:              200,
			outputState:         StateClosed,
			outputErr:           nil,
		},
		"\"state: half-open, consecutiveFailures: 0, expiry: after now, requests: 1, status: 200\"": {
			state:               StateHalfOpen,
			consecutiveFailures: 0,
			expiry:              time.Now().Add(time.Duration(1) * time.Hour),
			requests:            1,
			status:              200,
			outputState:         StateHalfOpen,
			outputErr:           ErrTooManyRequests,
		},
	}
	for testName, test := range tests {
		t.Logf("Running test case %s", testName)

		cb := NewCircuitBreaker("CircuitBreaker")
		cb.state = test.state
		cb.expiry = test.expiry
		cb.counts = Counts{
			ConsecutiveFailures: test.consecutiveFailures,
			Requests:            test.requests,
		}

		client := &mocks.MockClient{
			DoFunc: func(req *http.Request) (*http.Response, error) {
				body := "Hello world"
				response := http.Response{
					Status:     strconv.Itoa(test.status) + " Bad Request",
					StatusCode: test.status,
					Request:    req,
					Body:       ioutil.NopCloser(bytes.NewBufferString(body)),
				}
				return &response, nil
			},
		}
		err := cb.Do(
			func() error {
				req, err := http.NewRequest(
					"GET",
					"https://httpstat.us/400",
					nil,
				)
				if err != nil {
					return err
				}

				resp, err := client.Do(req)
				if err != nil {
					return err
				}
				if resp.StatusCode < 200 || resp.StatusCode > 299 {
					return errors.New(resp.Status)
				}
				return nil
			},
		)()
		if err != nil && err.Error() != test.outputErr.Error() {
			t.Errorf("error should be \"%v\", but is \"%v\"", test.outputErr, err)
		}
		if cb.state != test.outputState {
			t.Errorf("state should be %v, but is: %v", test.outputState, cb.state)
		}
	}
}
