# Go Stability library

A library that allows developers to use the Circuitbreaker- and Retry-pattern on requests.

## Usage

### Instantiating

The two stability-patterns can be used through the agent-struct:

```golang
package ...
import (
  ...
  stability "bitbucket.org/4s/go-stability"
)
...
agent := stability.NewAgent("Name")
...
```

by default, both of the patterns are enabled. To disable either of them, they are simply set to nil, as follows:

```golang
agent.SetCircuitBreaker(nil)
// or
agent.SetRetry(nil)
...
```

Each of the two patterns have a set of options that can be set, 
to match the needs of the project in which they are being used.
These can be set directly on either the CircuitBreaker or the Retry
of the agent as in the following example:

```golang
agent.CircuitBreaker.SetTimeout(value)
// or
agent.Retry.SetAttempts(value)
...
```

The available options are exposed as mutator methods on CircuitBreaker and Retry documented with godoc.

### Executing

When the settings are acceptable the request can be executed, by passing a function, 
in which the request is executed to the agent's Do-function as follows:

```golang
package ...
import (
  ...
  stability "bitbucket.org/4s/go-stability"
)
...
agent := stability.NewAgent("Name")
client := http.Client{}

var body []byte
respErr := agent.Do(
    func() error {
        // create request
        req, err := http.NewRequest(
            "GET",
            "http://example.org",
            nil,
        )
        if err != nil {
            // handle error
        }
        // execute request and get response
        resp, err = client.Do(req)
        if err != nil {
            // handle error
        }
        defer resp.Body.Close()

        body, err = ioutil.ReadAll(resp.Body)
        if err != nil {
            // handle error
        }
        return nil
    },
)
// use body
...
```